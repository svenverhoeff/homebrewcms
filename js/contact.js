/**
 * Contact.js - Client-side Validation on the contact page
 * 
 * @author Bugslayer
 * 
 */
$(document).ready(function() {
	/*
	 * Validates before form should be submitted. If returned true, the form
	 * will not be submitted.
	 */
	$("form").submit(function() {
		var error = validateLength($("#name_first"), 1);
		error = error & validateLength($("#name_last"), 1);
		error = error & validateEmail($("#email"));
		error = error & validateLength($("#subject"), 1);
		error = error & validateLength($("#message"), 10);
		if (error) {
			return true;
		} else {
			return false;
		}
	});

	/*
	 * Validates on blur of each field.
	 */
	$("#name_first").blur(function() {
		return validateLength($(this), 1);
	});
	$("#name_last").blur(function() {
		return validateLength($(this), 1);
	});
	$("#email").blur(function() {
		return validateEmail($(this));
	});
	$("#subject").blur(function() {
		return validateLength($(this), 1);
	});
	$("#message").blur(function() {
		return validateLength($(this), 10);
	});

});

// ========================= Reusable Validation Functions
// =========================

/**
 * Validates if a text has a minimum length.
 * 
 * @param field
 *            The input field
 * @param length
 *            minimum length
 */
function validateLength(field, length) {
	var fieldID = field.attr('id');
	var fieldInfo = $("#" + fieldID + 'ValResult');

	// Remove all styling
	field.removeAttr('class');
	fieldInfo.removeAttr('class');

	var tooSmall = field.val().length < length;
	if (tooSmall) {
		field.addClass("error");
		fieldInfo.text("Geen/te korte invoer (minimaal " + length + " tekens)");
		fieldInfo.addClass("error");
		return false;
	} else {
		field.addClass("success");
		fieldInfo.text("Correct");
		fieldInfo.addClass("success");
		return true;
	}
}

/**
 * Validates if a variable is an email address.
 * 
 * @param field
 *            The input field
 */
function validateEmail(field) {
	var fieldID = field.attr('id');
	var fieldInfo = $("#" + fieldID + 'ValResult');

	field.removeAttr('class');
	fieldInfo.removeAttr('class');

	// Get the position of @ and the last .
	var x = field.val();
	var atpos = x.indexOf("@");
	var dotpos = x.lastIndexOf(".");
	// Checks if @ and . are present and @ is positioned before .
	if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
		field.addClass("error");
		fieldInfo.text("Geen/ongeldig e-mailadres ingevoerd");
		fieldInfo.addClass("error");
		return false;
	} else {
		field.addClass("success");
		fieldInfo.text("Gevalideerd e-mailadres");
		fieldInfo.addClass("success");
		return true;
	}
}
